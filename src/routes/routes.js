
const { Router } = require('express');
const clienteController = require('../controllers/ClienteController');
const servidorController = require('../controllers/ServidorController');
const router = Router();

router.get('/clientes', clienteController.show);
router.get('/servidores', servidorController.show);
router.get('/clientes/:id', clienteController.index);
router.get('/servidores/:id', servidorController.index);
router.post('/cliente', clienteController.create);
router.post('/servidor', servidorController.create);
router.put('/clientes/:id', clienteController.update);
router.put('/servidores/:id', servidorController.update);
router.delete('/clientes/:id', clienteController.destroy);
router.delete('/servidores/:id', servidorController.destroy);

module.exports = router;

