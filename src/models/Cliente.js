const DataTypes = require("sequelize");
const sequelize = require("../config/sequelize");

const Cliente = sequelize.define('Cliente', {
    name: {
        type: DataTypes.STRING,
        allowNull: false
    },

    email: {
        type: DataTypes.STRING,
        allowNull: false
    },

    birth: {
        type: DataTypes.DATEONLY,
        allowNull: false
    },

    cpf: {
        type: DataTypes.INTEGER,
    },

    cep: {
        type: DataTypes.INTEGER,
    },

    num_tel: {
        type: DataTypes.INTEGER,
    },

    senha:{
        type: DataTypes.STRING,
        allowNull: false
    }
})

Cliente.associate = function(models){
    Cliente.hasMany(models.Servidor, {as: "historico", foreignKey: "histID"});
}

module.exports = Cliente;