const DataTypes = require("sequelize");
const sequelize = require("../config/sequelize");

const Servidor = sequelize.define('Servidor', {
    name: {
        type: DataTypes.STRING,
        allowNull: false
    },

    email: {
        type: DataTypes.STRING,
        allowNull: false
    },

    cpf: {
        type: DataTypes.INTEGER,
    },

    cep: {
        type: DataTypes.INTEGER,
    },

    num_tel: {
        type: DataTypes.INTEGER,
    },

    senha:{
        type: DataTypes.STRING,
        allowNull: false
    },

    especialidade:{
        type: DataTypes.STRING,
        allowNull: false
    }
})

Servidor.associate = function(models){
    Servidor.belongsToMany(models.Cliente, {through: "prestado", as:"servicosprestados"});
} 

module.exports = Servidor;