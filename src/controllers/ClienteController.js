const { response } = require('express');
const Cliente = require('../models/Cliente');

const create = async(req, res) => {
  try {
    const cliente = await Cliente.create(req.body);
    return res.status(201).json({message: "Cliente cadastrado com sucesso!", cliente: cliente});
  } catch (err) {
    res.status(500).json({error: err});
  }
};

const destroy = async(req, res) => {
  const {id} = req.params;
  try {
    const deleted = await Cliente.destroy({where: {id: id}});
    if(deleted){
      return res.status(200).json("Cliente deletado");
    }
    throw new Error();
  } catch (error) {
    return res.status(500).json("Cliente não encontrado");
  }
};

const show = async(req,res) => {
  try{
      const clientes = await Cliente.findAll(req.body);
      return res.status(200).json({clientes});
  }catch(err){
      return res.status(500).json({err});
  }
};

const index = async(req, res) => {
  const {id} = req.params;
  try {
    const cliente = await Cliente.findByPk(id);
    return res.status(200).json({cliente});
  } catch (err) {
    return res.status(500).json({err});
  }
};

const update = async(req, res) => {
  const {id} = req.params;
  try {
    const [updated] = await Cliente.update(req.body, {where:{id: id}});
    if(updated){
      const cliente = await Cliente.findByPk(id);
      return res.status(200).send(cliente);
    }
    throw new Error();
  } catch (err) {
    return res.status(500).json("Cliente não encontrado");
  }
};

module.exports = {
  create,
  show,
  index,
  update,
  destroy
};

