const { response } = require('express');
const Servidor = require('../models/Servidor');

const create = async(req, res) => {
    try {
        const servidor = await Servidor.create(req.body);
        return res.status(201).json({message: "Profissional cadastrado com sucesso!", servidor: servidor});
    } catch (err) {
        res.status(500).json({error: err});
    }
};

const destroy = async(req, res) => {
    const {id} = req.params;
    try {
    const deleted = await Servidor.destroy({where: {id: id}});
    if(deleted){
        return res.status(200).json("Profissional deletado");
    }
    throw new Error();
    } catch (error) {
        return res.status(500).json("Profissional não encontrado");
    }
};

const show = async(req,res) => {
    try{
        const servidores = await Servidor.findAll(req.body);
        return res.status(200).json({servidores});
    }catch(err){
        return res.status(500).json({err});
    }
};

const index = async(req, res) => {
    const {id} = req.params;
    try {
        const servidor = await Servidor.findByPk(id);
        return res.status(200).json({servidor});
    } catch (err) {
        return res.status(500).json({err});
    }
};

const update = async(req, res) => {
    const {id} = req.params;
    try {
        const [updated] = await Servidor.update(req.body, {where:{id: id}});
        if(updated){
            const servidor = await Servidor.findByPk(id);
            return res.status(200).send(servidor);
        }
        throw new Error();
    }catch (err) {
        return res.status(500).json("Cliente não encontrado");
    }
};

module.exports = {
    create,
    show,
    index,
    update,
    destroy
};

