const Servidor = require("../../models/Servidor");
const faker = require('faker-br');

    const ServidorSeeder = async function () {
    try {
        await Servidor.sync({ force: true });
        const servidores = [];

        for (let i = 0; i < 10; i++) {

        let servidor = await Servidor.create({
            email: faker.internet.email(),
            name: faker.name.firstName(),
            cpf: faker.br.cpf(),
            cep: faker.br.cpf(),
            num_tel: faker.phone.phoneNumber(),
            especialidade: faker.commerce.product(),
            senha: faker.internet.password(10),
            createdAt: new Date(),
            updatedAt: new Date()
        });

        /* if(i>1){
            let userFollowed = await User.findByPk(i-1);
            user.addFollowing(userFollowed);
        }*/
        }
    } catch (err) { console.log(err +'!'); }
}

module.exports = ServidorSeeder;