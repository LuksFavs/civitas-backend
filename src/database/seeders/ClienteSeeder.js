const Cliente = require("../../models/Cliente");
const faker = require("../../../node_modules/faker-br/index");

    const ClienteSeeder = async function () {
    try {
        await Cliente.sync({ force: true });
        const Clientes = [];

        for (let i = 0; i < 10; i++) {

        let cliente = await Cliente.create({
            email: faker.internet.email(),
            name: faker.name.firstName(),
            cpf: faker.br.cpf(),
            cep: faker.br.cpf(),
            num_tel: faker.phone.phoneNumber(),
            birth: new Date(),
            senha: faker.internet.password(10),
            createdAt: new Date(),
            updatedAt: new Date()
        });

        /* if(i>1){
            let userFollowed = await User.findByPk(i-1);
            user.addFollowing(userFollowed);
        }*/
        }
    } catch (err) { console.log(err +'!'); }
}

module.exports = ClienteSeeder;