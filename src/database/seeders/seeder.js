require('../../config/dotenv')();
require('../../config/sequelize');

const clienteSeeder = require('./ClienteSeeder');
const servidorSeeder = require('./ServidorSeeder');

(async () => {
  try {
    await clienteSeeder();
    await servidorSeeder();

  } catch(err) { console.log(err) }
})();
